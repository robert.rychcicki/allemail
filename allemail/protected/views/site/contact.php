<?php

include("klasybazy.php");

$options = new ezcMailImapTransportOptions();
$options->ssl = true;
$imap = new ezcMailImapTransport( "poczta.o2.pl", 993, $options );
// Authenticate to the IMAP server
$imap->authenticate( "praktyki3b@o2.pl", "testowe123" );
// Select the Inbox mailbox
$imap->selectMailbox( 'Inbox' );
// Usually you will call one of these fetch functions:
// Fetch all messages on the server
$set = $imap->fetchAll();
// Create a new mail parser object
$parser = new ezcMailParser();
// Parse the set of messages retrieved from the server earlier
$mail = $parser->parseMail( $set );

$this->pageTitle=Yii::app()->name;

for($i = 0; !empty($mail[$i]); $i++)
{
    $mailbody = $mail[$i]->subject;
    $mailbody = Zdanie($mailbody, "Zakończenie", " licytacji");

    if(!empty($mailbody))
    {
        $mailbody = $mail[$i]->generateBody();

        preg_match_all('/Kupujący\*(.[^<]*)\</s', $mailbody, $wynik);
        preg_match_all('/zł(.[^<]*)/s', $wynik[1][0], $wynik);

        if(strstr($wynik[1][0], "\n")!=False)
            $nick = explode("\n", $wynik[1][0]);
        elseif(strstr($wynik[1][0], " ")!=False)
            $nick = explode(" ", $wynik[1][0]);
        $nick = $nick[1];

        if(kupujacy::jaki_ma_id($nick) == NULL)
        {
            $kupujacy = new kupujacy();
            $kupujacy->nazwakupujacego=$nick;
            $kupujacy->save();
        }

        $towar = new towary;
        preg_match_all('/Nazwa\*(.[^<]*)\</s', $mailbody, $wynik);
        $towar->nazwatowaru=$wynik[1][0];
        preg_match_all('/Kupujący\*(.[^<]*)\</s', $mailbody, $wynik);
        preg_match_all('/([0-9]+)/s', $wynik[1][0], $wynik);
        $towar->ilosc=$wynik[0][0];
        preg_match_all('/([0-9]+),([0-9])([0-9])/s', $mailbody, $wynik);
        $wynik[0][1]=str_replace(",", ".", $wynik[0][1]);
        $towar->cenatowaru=$wynik[0][1];
        $towar->czywyslane=FALSE;
        $towar->czyzaplacone=FALSE;
        $towar->danedowysylki="BRAK";
        $towar->danefaktury="BRAK";
        preg_match_all('/sprzedaży\*(.[^<]*)\*Cena /s', $mailbody, $wynik);
        $towar->datazakupu=$wynik[1][0];
        $towar->numerlistuprzewozowego='0';
        $towar->sposobprzesylki="BRAK";

        $towar->id_kupujacego=kupujacy::jaki_ma_id($nick);

        $towar->save();
    }
}

for($i = 0; !empty($mail[$i]); $i++)
{
    $mailbody = $mail[$i]->subject;
    $mailbody = Zdanie($mailbody, "Sprzedałeś", " przez");

    if(!empty($mailbody))
    {
        $mailbody = $mail[$i]->generateBody();

        preg_match_all('/ceg(.[^<]*)\</s', $mailbody, $wynik);
        if(strstr($wynik[1][0], "\n")!=False)
            $nick = explode("\n", $wynik[1][0]);
        elseif(strstr($wynik[1][0], " ")!=False)
            $nick = explode(" ", $wynik[1][0]);
        $nick = $nick[1];

        if(kupujacy::jaki_ma_id($nick) == NULL)
        {
            $kupujacy = new kupujacy();
            $kupujacy->nazwakupujacego=$nick;
            $kupujacy->save();
        }

        $towar = new towary;
        preg_match_all('/Nazwa\*(.[^<]*)\</s', $mailbody, $wynik);
        $towar->nazwatowaru=$wynik[1][0];
        preg_match_all('/\*([0-9]+)\*/s', $mailbody, $wynik);
        $towar->ilosc=$wynik[1][0];
        preg_match_all('/([0-9]+),([0-9])([0-9])/s', $mailbody, $wynik);
        $wynik[0][1]=str_replace(",", ".", $wynik[0][1]);
        $towar->cenatowaru=$wynik[0][1];
        $towar->czywyslane=FALSE;
        $towar->czyzaplacone=FALSE;
        $towar->danedowysylki="BRAK";
        $towar->danefaktury="BRAK";
        preg_match_all('/Data:(.[^<]*)Temat:/s', $mailbody, $wynik);
        $towar->datazakupu=$wynik[1][0];
        $towar->numerlistuprzewozowego='0';
        $towar->sposobprzesylki="BRAK";

        $towar->id_kupujacego=kupujacy::jaki_ma_id($nick);

        $towar->save();
    }
}

for($i = 0; !empty($mail[$i]); $i++)
{
    $mailbody = $mail[$i]->subject;
    $mailbody = Zdanie($mailbody, "Kupujący", " wybrał");

    if(!empty($mailbody))
    {
        $wynik = explode(" ", $mailbody);

        $nick = $wynik[1];
        $id = kupujacy::jaki_ma_id($nick);

        $mailbody = $mail[$i]->generateBody();

        preg_match_all('/Transakcja\*(.[^<]*)\</s', $mailbody, $wynik);

        for($j = 0; towary::szukanie_id($id, $j)!=NULL; $j++)
        {
            $idtowaru = towary::szukanie_id($id, $j);
            $towar = towary::model()->findByPk($idtowaru);

            if($wynik[1][0]==$towar->nazwatowaru)
            {
                preg_match_all('/Allegro\s*(.[^<]*)\*F/s', $mailbody, $wynik);
                preg_match_all('/Telefon\*(.[^<]*)\*W/s', $mailbody, $tel);
                if(empty($wynik[1][0]))
                    $towar->danedowysylki='tel. '.$tel[1][0];
                else
                    $towar->danedowysylki=$wynik[1][0].'<br>tel. '.$tel[1][0];
                preg_match_all('/VAT\*(.[^<]*)\*T/s', $mailbody, $wynik);
                $towar->danefaktury=$wynik[1][0];
                preg_match_all('/cennika\sdostawy\s*\*([^<*]+)[^*]*/ims', $mailbody, $wynik);
                $towar->sposobprzesylki=$wynik[1][0];
                $towar->save();
            }
        }
    }
}

for($i = 0; !empty($mail[$i]); $i++)
{
    $mailbody = $mail[$i]->subject;
    $mailbody = Zdanie($mailbody, "Nowa", " wpłata");

    if(!empty($mailbody))
    {
        //$wynik = explode(" ", $mailbody);

        $mailbody = $mail[$i]->generateBody();

        preg_match_all('/Płacący\*(.[^<]*)\,/s', $mailbody, $wynik);

        $nick = trim($wynik[1][0]);

        preg_match_all('/([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/s', $mailbody, $wynik);

        $email = $wynik[0][2];

        $id = kupujacy::jaki_ma_id($nick);

        preg_match_all('/Transakcja\s*\*(.[^<]*)\</s', $mailbody, $wynik);

        for($j = 0; towary::szukanie_id($id, $j)!=NULL; $j++)
        {
            $idtowaru = towary::szukanie_id($id, $j);
            $towar = towary::model()->findByPk($idtowaru);

            if($wynik[1][0]==$towar->nazwatowaru)
            {
                $towar->danedowysylki=$towar->danedowysylki.'<br>'.$email;
                $towar->czyzaplacone=TRUE;
                $towar->save();
            }
        }
    }
}

echo "MAILE POBRANIE!";