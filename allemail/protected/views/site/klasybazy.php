<?php

class kupujacy extends CActiveRecord
{
    public $id;
    public $nazwakupujacego;
    public $uwagi;

    public static function jaki_ma_id($nick)
    {
        /** @var kupujacy $kupujacy */
        $kupujacy = self::model()->find('nazwakupujacego=:nick', array(':nick'=>$nick));
        return !empty($kupujacy) ?$kupujacy->id :null;
    }

    public function relations()
    {
        return array('id'=>array(self::HAS_MANY, 'towary', 'id_kupujacego'),);
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}

class towary extends CActiveRecord
{
    public $id;
    public $nazwatowaru;
    public $ilosc;
    public $cenatowaru;
    public $datazakupu;
    public $sposobprzesylki;
    public $danedowysylki;
    public $danefaktury;
    public $czyzaplacone;
    public $czywyslane;
    public $numerlistuprzewozowego;
    public $id_kupujacego;

    public static function szukanie_id($id, $kolejnosc)
    {
        /** @var towary $towary */
        $towary = self::model()->findALL('id_kupujacego=:id', array(':id'=>$id));
        return !empty($towary[$kolejnosc]) ?$towary[$kolejnosc]->id :null;
    }

    public function relations()
    {
        return array('id_kupujacego'=>array(self::BELONGS_TO, 'kupujacy', 'id'),);
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}

function Zdanie($mailbody, $poczatek, $koniec)
{
    $str = strstr($mailbody, $poczatek);
    return substr($str, 0, strpos($str, $koniec));
}


/* @var $this SiteController */
$connection=Yii::app()->db;