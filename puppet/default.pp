
exec { 'apt-get update':
	command => '/usr/bin/apt-get update'
}

package {
	'php5':
		ensure => installed,
		require => Exec['apt-get update']
}

package { 
	[
		'nginx', 
		'php5-cli', 'php5-xdebug',
		'php5-curl', 'php5-gd', 'php5-mysql',
		'php5-fpm'
	]:
	ensure => present,
	require => Exec['apt-get update']
}

package { 'mysql-client':
	ensure => present,
	require => Exec['apt-get update']
}

package { 'mysql-server':
	ensure => present,
	require => Exec['apt-get update']
}


file { 'nginx-conf':
    path => '/etc/nginx/nginx.conf',
    ensure => file,
    require => Package['nginx'],
    source => '/vagrant/puppet/nginx/nginx.conf',
}

file { 'vagrant-nginx':
    path => '/etc/nginx/sites-available/vagrant',
    ensure => file,
    require => Package['nginx'],
    source => '/vagrant/puppet/nginx/site-vagrant',
}

file { 'default-nginx-disable':
    path => '/etc/nginx/sites-enabled/default',
    ensure => absent,
    require => Package['nginx'],
}

file { 'vagrant-nginx-enable':
    path => '/etc/nginx/sites-enabled/vagrant',
    target => '/etc/nginx/sites-available/vagrant',
    ensure => link,
    notify => Service['nginx'],
    require => [
        File['vagrant-nginx'],
        File['default-nginx-disable'],
    ],
}

file { 'vagrant-mysql':
    source => '/vagrant/puppet/mysql/vagrant.cnf',
    path => '/etc/mysql/conf.d/vagrant.cnf',
    ensure => file,
    mode => '0644',
    owner => 'root',
    group => 'root',
    notify => Service['mysql'],
    require => [
        Package['mysql-server'],
    ],
}

file { 'php5-fpm':
    source => '/vagrant/puppet/php/fpm-vagrant.conf',
    path => '/etc/php5/fpm/pool.d/vagrant.conf',
    ensure => file,
    notify => Service['php5-fpm'],
    require => [
        Package['php5-fpm'],
    ],
}

file { 'php5-xdebug':
    content => template('/vagrant/puppet/php/xdebug-vagrant.ini'),
    path => '/etc/php5/fpm/conf.d/xdebug-vagrant.ini',
    ensure => file,
    notify => Service['php5-fpm'],
    require => [
        Package['php5-fpm'],
    ],
}


service { 'nginx':
	ensure => running,
	require => Package['nginx'],
}

service { 'php5-fpm':
	ensure => running,
	require => Package['php5-fpm'],
}

service { 'mysql':
	ensure => running,
	require => Package['mysql-server'],
}

# exec { 'service mysql restart':
# 	command => '/usr/bin/service mysql restart'
# }

exec { 'mysql set root':
	command => '/usr/bin/mysql -u root < /vagrant/puppet/mysql/default.sql',
	require => Package['mysql-server'],
}

exec { 'mysql loaddatabase root':
	command => '/usr/bin/mysql -u root < /vagrant/puppet/mysql/default.sql',
	require => Package['mysql-server'],
}

